using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundWave : MonoBehaviour
{
    public float angleDirection = 0f;
    public float angle = 0f;
    public string emitter;
    public float forceStart = 0f;
    public float decay = 2f;
    public float speed = 2f;
    public bool isLineRendered = true;

    private float _forceCurrent;
    private float _distanceCurrent;
    private LineRenderer _lr;
    private EdgeCollider2D _ec;

    //[SerializeField] private float _decayThreshold = 0.0f;
    private List<Vector2> _points = new List<Vector2>();

    void Start()
    {
        _forceCurrent = forceStart;
        _distanceCurrent = 0f;

        _lr = gameObject.AddComponent<LineRenderer>();
        _lr.useWorldSpace = false;
        _lr.material = new Material(Shader.Find("Legacy Shaders/Particles/Alpha Blended Premultiply"));
        _lr.startWidth = 0.1f;
        _lr.endWidth = 0.1f;

        _ec = gameObject.AddComponent<EdgeCollider2D>();
        _ec.isTrigger = true;
    }

    void Update()
    {
    }

    void FixedUpdate()
    {
        if (isLineRendered)
        {
            CreateLine();
        }
        CreateCollider();

        _forceCurrent -= decay * Time.fixedDeltaTime;
        _distanceCurrent += speed * Time.fixedDeltaTime;
        if (_forceCurrent <= 0f)
        {
            transform.parent.GetComponent<Sound>().childrenCount--;
            Destroy(gameObject);
        }
    }

    private void CreateLine()
    {
        _points.Clear();

        float angleDelta = angle / 2;
        _lr.positionCount = 0;
        _lr.positionCount = 2;

        _lr.startColor = new Color(0.7f, 1, 1, _forceCurrent/forceStart);
        _lr.endColor = new Color(0.7f, 1, 1, _forceCurrent / forceStart);

        float z = 0f;
        float x1 = Mathf.Sin(Mathf.Deg2Rad * (angleDirection + angleDelta)) * _distanceCurrent;
        float y1 = Mathf.Cos(Mathf.Deg2Rad * (angleDirection + angleDelta)) * _distanceCurrent;
        float x2 = Mathf.Sin(Mathf.Deg2Rad * (angleDirection - angleDelta)) * _distanceCurrent;
        float y2 = Mathf.Cos(Mathf.Deg2Rad * (angleDirection - angleDelta)) * _distanceCurrent;

        _lr.SetPosition(0, new Vector3(x1, y1, z));
        _lr.SetPosition(1, new Vector3(x2, y2, z));

        _points.Add(new Vector2(x1, y1));
        _points.Add(new Vector2(x2, y2));
    }

    private void CreateCollider()
    {
        _ec.points = _points.ToArray();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name != emitter)
        {
            _forceCurrent /= 2;
        }
    }
}
