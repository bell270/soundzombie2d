using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public bool isPaused = false;

    [Header("Movement Variables")]
    CharacterTopDownController controller;
    private Vector2 _lookAt;
    private Vector2 _movement;
    [SerializeField] private float _runningMultiplier = 2f;
    [SerializeField] private float _crouchingMultiplier = 0.5f;
    [SerializeField] private bool _isRunning = false;
    [SerializeField] private bool _isCrouching = false;

    private float _stepTimer = 0f;
    [SerializeField] private float _stepFrequancy = 1f;

    void Start()
    {
        controller = GetComponent<CharacterTopDownController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPaused)
        {
            Vector2 mousePosition = Input.mousePosition;
            _lookAt = Camera.main.ScreenToWorldPoint(mousePosition);
            _movement = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

            _isCrouching = (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl));
            _isRunning = !_isCrouching && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift));

            if (Input.GetMouseButtonDown(0))
            {
                FindObjectOfType<AudioManager>().PlayClip("Shot");
                StartCoroutine(FindObjectOfType<CameraShake>().Shake(.15f, .4f));
            }
            if (Input.GetMouseButtonDown(1))
            {
                Time.timeScale = 0;
                isPaused = true;
                SceneManager.LoadScene(SceneManager.sceneCountInBuildSettings - 1, LoadSceneMode.Additive);
            }

        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(isPaused)
            {
                isPaused = false;
                Time.timeScale = 1;
                _ = SceneManager.UnloadSceneAsync(1);
            }
            else
            {
                isPaused = true;
                Time.timeScale = 0;
                SceneManager.LoadScene(1, LoadSceneMode.Additive);
            }

        }
    }

    private void FixedUpdate()
    {
        controller.Move(_movement * (_isCrouching ? _crouchingMultiplier : 1) * (_isRunning ? _runningMultiplier : 1));
        controller.LookAt(_lookAt);

        _stepTimer += Time.fixedDeltaTime;
        if (_movement.magnitude > 0
            && _stepTimer > (1f / (_stepFrequancy * (_isCrouching ? _crouchingMultiplier : 1) * (_isRunning ? _runningMultiplier : 1)))
        )
        {
            EmitSound();
            _stepTimer = 0f;
        }
    }

    private void EmitSound()
    {
        Sound sound = (new GameObject(gameObject.name + "_sound")).AddComponent<Sound>();
        sound.gameObject.transform.SetPositionAndRotation(transform.position, transform.rotation);
        sound.emitter= gameObject.name;
        sound.segmentsAmount = 12;
        sound.forceStart = 5;
        sound.SendWave();
    }
}
