using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    public int segmentsAmount = 3;
    public string emitter;
    public float forceStart = 0f;
    public bool isLineRendered = true;
    [SerializeField] private float decay = 2f;
    [SerializeField] private float speed = 2f;

    public int childrenCount;

    public void SendWave()
    {
        float angle = 360f / segmentsAmount;
        for(int i = 0; i < segmentsAmount; i++)
        {
            SoundWave newWave = (new GameObject("wave_" + i)).AddComponent<SoundWave>();
            newWave.transform.parent = transform;
            newWave.transform.localPosition = Vector3.zero;
            childrenCount++;
            newWave.angle = angle;
            newWave.angleDirection = i * angle;
            newWave.emitter = emitter;
            newWave.decay = decay;
            newWave.speed = speed;
            newWave.forceStart = forceStart;
        }
    }

    private void Update()
    {
        if (childrenCount == 0)
        {
            Destroy(gameObject);
        }
    }
}
