using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterTopDownController : MonoBehaviour
{
    private Rigidbody2D _rb;

    [Header("Movement Variables")]
    public float speedMultiplier = 0f;
    public float rotationSpeed = 0f;
    public float maxMagnitude = 0f;
    [Space(10)]

    private Vector2 _movement = Vector2.zero;
    private Vector2 _lookAt = Vector2.zero;

    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    public void Move(Vector2 movement)
    {
        _rb.MovePosition(_rb.position + speedMultiplier * movement * Time.fixedDeltaTime);
    }

    public void LookAt(Vector2 lookAt)
    {
        Vector2 direction = lookAt - new Vector2(transform.position.x, transform.position.y);
        float angle = Vector2.SignedAngle(transform.right, direction);
        float angleDiff = angle - _rb.rotation;
        float newAngle = (_rb.rotation + rotationSpeed * angle * Time.fixedDeltaTime) % 360;
        _rb.rotation = newAngle;
    }

    public void MoveTo(Vector2 moveTo)
    {
        Vector2 movement = moveTo - new Vector2(transform.position.x, transform.position.y);
        _rb.MovePosition(_rb.position + speedMultiplier * movement * Time.fixedDeltaTime);
    }

    public void LookTo(Vector2 lookToDirection)
    {
        float angle = Vector2.SignedAngle(transform.right, lookToDirection);
        float angleDiff = angle - _rb.rotation;
        float newAngle = (_rb.rotation + rotationSpeed * angle * Time.fixedDeltaTime) % 360;
        _rb.rotation = newAngle;
    }
}
